/* Given 1 <= n <= 32767, throw an n-sided die.
 * If n is outside the range, return -1.
 */
int roll_die(int n);

/* Roll the given number of dice and return their sum. */
int sumdice(int ndice, int sides);

/* For the specified number of dice throws: calculate the sum of the
   given number of dice, and increment the array slot whose index is
   the sum.
*/
void         fillsums(int sums[], int ndice, int sides, int maxrolls);

/* Print the number of occurrences of the various sums. */
void         printsums(int sums[], int begin_limit, int end_limit);

/* Print a horizontal bar histogram for a given sum. */
void         print_histogram(int sum, int n, int scale);

/* Print a histogram of the occurrences. */
extern void  histsums(int sums[], int begin_limit, int end_limit);
