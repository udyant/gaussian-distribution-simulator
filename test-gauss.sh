#!/bin/sh

./gauss
if [ $? -ne 0 ];
then
    echo 'Issue with no argument.'
fi
echo

./gauss 0
if [ $? -ne 0 ];
then
    echo 'issue with single argument 0.'
fi
echo

./gauss 1
if [ $? -ne 0 ];
then
    echo 'issue with single argument 1.'
fi
echo

./gauss 2
if [ $? -ne 0 ];
then
    echo 'issue with single argument 2.'
fi
echo

./gauss 0 0
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 0 0.'
fi
echo

./gauss 0 1
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 0 1.'
fi
echo

./gauss 1 0
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 1 0.'
fi
echo

./gauss 1 1
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 1 1.'
fi
echo

./gauss 1 2
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 1 2.'
fi
echo

./gauss 2 1
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 2 1.'
fi
echo

./gauss 2 2
if [ $? -ne 0 ];
then
    echo 'issue with two arguments 2 2.'
fi

