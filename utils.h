/* Pick a number in [MIN, MAX] with uniform probability, where
 *             0 <= min <= max <= 32767
 * Adapted from randrange() by Richard Heathfield.
 */
int uniform_int_in(int min, int max);

