CC = gcc
CFLAGS = -std=c11 -pedantic-errors -Wall -Wextra -Werror -O0 -g
OBJS = gauss.o main.o utils.o
HEADERS = gauss.h utils.h

gauss: $(OBJS) $(HEADERS) depend
	$(CC) $(CFLAGS) -o gauss $(OBJS)

depend:
	$(CC) $(CFLAGS) -E -MM *.c > .depend

include .depend

tags:
	etags *.[ch] > TAGS

clean:
	rm -f *.o gauss
