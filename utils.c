#include "utils.h"

#include <stdlib.h>

/* Picks a number in [1, n] with uniform probability. */
/*
 * unsigned int uniform_int_in(unsigned int min, unsigned int max)
 * {
 *   return min + ((unsigned) rand() / (RAND_MAX / max + 1));
 * }
 */

int uniform_int_in(int min, int max)
/* Pick a number in [MIN, MAX] with uniform probability, where
 *             0 <= min <= max <= 32767
 * Adapted from randrange() by Richard Heathfield.
 */
{
    if (min > max)
    {
	int t = min;
	min = max;
	max = t;
    }
    if (!(0 <= min && max <= 32767))
    {
	return -1;
    }
    return min + (max - min + 1) * ((unsigned long) rand() / (RAND_MAX + 1.0));
}
