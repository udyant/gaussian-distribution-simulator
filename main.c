#include "gauss.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROLLS    100000           /* The number of throws of the dice */

int main(int argc, char *argv[])
/* Return codes:
 * 0 -- success
 * 1 -- usage error
 * 2 -- out of memory
 * 3 -- argument is not a number
 */
{
    int ndice;
    int sides;
    unsigned int limit;
    int *sums;
    char *ndice_buffer = argv[1];        /* Single digit dice */
    char *sides_buffer = argv[2];        /* Single digit sides */
    char *end;

    /* There should be two arguments. */
    if (argc < 3)
    {
	printf("usage: %s <number of dice> <number of sides>\n", argv[0]);
	puts("Please give both arguments.");
	return 1;
    }

    /* Parse the number of dice to throw... */
    ndice = (unsigned int) (strtoul(ndice_buffer, &end, 10));
    if (*end != '\0')
    {
	fprintf(stderr, "`%s` is not a valid number of dice.\n", ndice_buffer);
	return 3;
    }
    /* ... and sanitize it. */
    if (ndice < 1)
    {
	fputs("One cannot have absurd dice.\n", stderr);
	return 1;
    }

    /* Parse the number of sides each dice has... */
    sides = (unsigned int) (strtoul(sides_buffer, &end, 10));
    if (*end != '\0')
    {
	fprintf(stderr, "`%s` is not a valid number of sides.\n", sides_buffer);
	return 3;
    }
    /* ... and sanitize it. */
    if (sides < 2)
    {
	fputs("One cannot have absurd sides.\n", stderr);
	return 1;
    }

    srand((unsigned) time(NULL));        /* Seed the RNG using the
					  * current time.
					  */
    limit = sides * ndice + 1;
    sums = calloc(limit, sizeof(int));   /* We use only indices ndice to
					  * limit - 1 for indexing the
					  * sums of the given of dice.
					  * We waste
					  *    sizeof(int) * NDICE
					  * bytes.
					  */
    if (sums == NULL)
    {
	fprintf(stderr, "Unable to obtain %lud bytes of memory for holding sums.\n",
		limit * sizeof(int));
	return 2;
    }

    fillsums(sums, ndice, sides, ROLLS); /* Count the sums. */
    printsums(sums, ndice, limit);       /* Print how many of each sum
					  * counted.
					  */
    putchar('\n');
    histsums(sums, ndice, limit);        /* Print a histogram of
					  * the same.
					  */

    free(sums);			         /* Release the memory used for
					  * holding the sums. */

    return 0;                            /* Done.  So, exit. */
}
