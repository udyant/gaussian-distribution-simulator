#include "gauss.h"
#include "utils.h"

#include <stdio.h>

#define SCALE    200              /* Used to fit the histogram into the
                                     screen width.  A lower value
                                     implies longer bars.
                                  */

/* Given 1 <= n <= 32767, throw an n-sided die.
 * If n is outside the range, return -1.
 */
int roll_die(int n)
{
    if (!(1 <= n && n <= 32767))
    {
	return -1;
    }
    return uniform_int_in(1, n);
}

/* Roll the given number of dice and return their sum. */
int sumdice(int ndice, int sides)
{
    int i;
    int sum = 0;

    for (i = 0; i < ndice; i++)
    {
	sum += roll_die(sides);
    }

    return sum;
}

/* For the specified number of dice throws: calculate the sum of the
 * given number of dice, and increment the array slot whose index is the
 * sum.
 */
void fillsums(int sums[], int ndice, int sides, int maxrolls)
{
    int i;

    for (i = 0; i < maxrolls; i++)
    {
	++sums[sumdice(ndice, sides)];
    }
}

/* Print the number of occurrences of the various sums. */
void printsums(int sums[], int begin_limit, int end_limit)
{
    int i;

    for (i = begin_limit; i < end_limit; i++)
    {
	printf("%2u: %d\n", i, sums[i]);
    }
}

/* Print a horizontal bar histogram for a given sum. */
void print_histogram(int sum, int n, int scale)
{
    int i;

    printf("%2u: ", sum);
    for (i = 0; i < n / scale; i++)
    {
	putchar('=');
    }
    putchar('\n');
}

/* Print a histogram of the occurrences. */
void histsums(int sums[], int begin_limit, int end_limit)
{
    int i;

    for (i = begin_limit; i < end_limit; i++)
    {
	print_histogram(i, sums[i], SCALE);
    }
}
