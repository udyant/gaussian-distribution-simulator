A six sided die has, obviously, six faces.  The probability of a
particular face turning up on a given throw is 1/6 or about 16.67%.  Its
distribution curve is not very exciting to look at, no matter how many
times you throw the die.

Now, when we have two dice, things get rather more interesting.
Specifically, consider the sum of the two top faces on a given throw.
As each die is six-sided (or, "is a d6", as they say), there are

                         6x6 = 36

possible combinations.  We have known this since school.  We also know
that 7 as the sum has the maximum possible combinations, 6 in 36.
(Listing them out is left as an exercise to the bored and depressed
reader.)  So, while 7 has a probability of 16.67% of turning up, a
fringe value such as 2 or 12 has a relatively low 2.78% probability.

The least value on a d6 is 1; the biggest is 6.
For two dice, the least _sum_ is 2x1 = 2; the biggest _sum_ is
2x6 = 12.
For three dice: least is 3x1 = 3; biggest is 3x6 = 18.
.
.
.
For n dice: least is nx1 = n; biggest is nx6 = 6n.

Consider the mean of the least and biggest for each:

2: (2 + 12) / 2
    = 14 / 2
    = 7

3: (3 + 18) / 2
    = 21 / 2
    = 10.5

4: (4 + 24) / 2
    = 28 / 2
    = 14

n: (n + 6n) / 2
    = 7n / 2
    = 3.5n

This means that if we plot a graph for the number of occurrences of the
various sums for n = 2, 3, 4, ..., we will get what is called a Bell
curve.  A .5 in the mean means that the central peak of the graph is not
fixed and mostly oscillates between the floor and ceiling of the mean,
depending on the given experimental run.




This is a diminutive Gaussian distribution program.  For a given number
of dice, first it prints the number of occurrences of each sum of the
dice, then it draws a histogram.

It is written in ISO '90 C, or ANSI '89 C.  It is heavily commented and
uses what seems to be a sane layout for a program, given the current
level of knowledge of the program author:

* Makefile - Rules for building the program.  Also, using which
  compiler.
* utils.h  - Stuff that could be used elsewhere for other programs.
* gauss.h  - The core functionality of the program.
* main.c   - The driver.  `main' houses nothing of importance by
  itself.

An unexplored aspect of the program design is the scaling of the
histogram.  Right now, with 10000 rolls of the dice, the occurrences per
sum is divided by 20 to fit the bars comfortably within the screen
width.  It was found that division by 10 sufficed for 1000 rolls, but
that division by 400 (a much larger scaling factor) was needed for
100000.

What is required, the author believes, is a formula to get a good
scaling factor given the number of rolls.
